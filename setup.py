import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="google_drive_folder_copy",
    version="0.0.1",
    author="Colin Thibadeau",
    author_email="colin.thibadeau@gmail.com",
    description="A small example package",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/sampleproject",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=[
        'google-api-python-client==1.11.0',
        'google-auth-httplib2==0.0.4',
        'google-auth-oauthlib==0.4.1',
        'oauth2client==4.1.3'
    ]
)
