from __future__ import print_function

from copy import deepcopy

from googleapiclient.discovery import build
from google.oauth2 import service_account

SCOPES = [
    'https://www.googleapis.com/auth/drive',
    'https://www.googleapis.com/auth/drive.appdata',
    'https://www.googleapis.com/auth/drive.file',
    'https://www.googleapis.com/auth/drive.photos.readonly'
]

FILE_FIELDS = 'kind, id, name, mimeType, parents'


def client(path_to_json_credentials):
    credentials = service_account.Credentials.from_service_account_file(path_to_json_credentials, scopes=SCOPES)
    client = build('drive', 'v3', credentials=credentials)
    return client


def copy(client, original_folder, destination_folder):
    original_folder_list = client.files().list(q=f"'{original_folder}' in parents").execute()
    for f in original_folder_list['files']:
        print(f)
        if f['mimeType'] == 'application/vnd.google-apps.folder':
            print(f'copying folder {f["name"]}')
            body = deepcopy(f)
            del body['id']
            body['parents'] = [destination_folder]
            new_folder = client.files().create(body=body, fields=FILE_FIELDS).execute()
            print(new_folder)
            copy(client, f['id'], new_folder['id'])
            continue
        print(f'copying file {f["name"]}')
        body = {
            'parents': [destination_folder],
            'name': f['name']
        }
        new_file = client.files().copy(fileId=f['id'], body=body, fields=FILE_FIELDS).execute()
        print(new_file)
    return client
